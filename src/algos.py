import copy
from check import *
import time

def backtrack (board: sboard) -> sboard:
    fixed_cells = get_fixed_cells(board)
    is_backtracking = False
    y = 0
    while y < 9:
        x = 0
        while x < 9:
            # Sleep a frame so that Tkinter can update!
            time.sleep(0.00001)
            if (y, x) not in fixed_cells:
                while True:
                    if board[y][x] < 9:
                        # Try cell till 9
                        is_backtracking = False
                        board[y][x] += 1
                    else:
                        # Backtrack
                        is_backtracking = True
                        board[y][x] = 0
                        while True:
                            if y == 0 and x == 0: # No solution (Trying to backtrack 1st row, 1st col)
                                return None
                            elif x == 0: # First col
                                y -= 1
                                x = 8
                            else: # Normal case
                                x -= 1
                            if (y, x) not in fixed_cells: break
                    if check_board(board, True) or is_backtracking: break
            if not is_backtracking: x += 1
        if not is_backtracking: y += 1
    return board

def get_fixed_cells (board: sboard) -> [(int, int)]:
    indexes = []
    for i in range(9):
        for j in range(9):
            if board[i][j] != 0:
                indexes.append((i, j))
    return indexes

def print_board (board: sboard):
    for row in board:
        for cell in row:
            if cell == 0:
                print(' ', end=' ')
            else:
                print(cell, end=' ')
        print()


# easy_board = [
#     [5,3,0,0,7,0,0,0,0],
#     [6,0,0,1,9,5,0,0,0],
#     [0,9,8,0,0,0,0,6,0],
#     [8,0,0,0,6,0,0,0,3],
#     [4,0,0,8,0,3,0,0,1],
#     [7,0,0,0,2,0,0,0,6],
#     [0,6,0,0,0,0,2,8,0],
#     [0,0,0,4,1,9,0,0,5],
#     [0,0,0,0,8,0,0,7,9]
# ]
# backtrack(easy_board)
